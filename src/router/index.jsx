import React from 'react'
import { createBrowserRouter } from 'react-router-dom'
import Home from '../components/pages/Home'
import About from '../components/pages/About'
import Products from '../components/pages/Products'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/about',
    element: <About></About>
  },
  {
    path: '/products',
    element: <Products />
  }
])

export default router
