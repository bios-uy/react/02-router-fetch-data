import React, { useEffect, useState } from 'react'
import Product from '../../organisms/Product'

// Styles
import styles from './styles.module.css'

console.log(styles)

const Products = () => {
  const [listProducts, setListProducts] = useState([])

  const getProducts = async () => {
    const res = await fetch('https://dummyjson.com/products')
    if (!res.ok) throw new Error('Fallo nuestra API')
    const data = await res.json()
    setListProducts(data.products)
  }

  useEffect(() => {
    getProducts()
  }, [])

  return (
    <div>
      <h1 className={styles.mainTitle}>Hola a mi tienda</h1>

      <div className={styles.listProducts}>
        {listProducts.map((product) => (
          <Product
            key={`product-${product.id}`}
            thumbnail={product.thumbnail}
            title={product.title}
          />
        ))}
      </div>
    </div>
  )
}

export default Products
