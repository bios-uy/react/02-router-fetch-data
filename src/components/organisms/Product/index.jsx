import React from 'react'
import PropTypes from 'prop-types'

import style from './styles.module.css'

const Product = ({ thumbnail, title }) => {
  return (
    <div className={style.product}>
      <img
        className={style.productThumbnail}
        src={thumbnail}
        alt="image product"
      />
      <div className={style.productDetails}>
        <span className={style.productDetailsTitle}>{title}</span>
      </div>
    </div>
  )
}

Product.propTypes = {
  thumbnail: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default Product
